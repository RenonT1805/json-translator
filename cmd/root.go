/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"github.com/gosuri/uiprogress"
	"github.com/spf13/cobra"
	"gitlab.com/RenonT1805/json-translator/cmd/validator"
	"gitlab.com/RenonT1805/json-translator/translator"
)

type _options struct {
	File           string `validate:"required"`
	Output         string
	Query          string `validate:"required"`
	Translator     string
	SourceLanguage string
	TargetLanguage string
	MaxParallel    int
}

var options _options

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "json-translator",
	Short: "",
	Long:  ``,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		return validator.DefaultValidation(options)
	},
	RunE: runRootCmd,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringVarP(&options.File, "file", "f", "", "翻訳対象ファイル")
	rootCmd.Flags().StringVarP(&options.Output, "output", "o", "translated.json", "翻訳済みデータの出力ファイル")
	rootCmd.Flags().StringVarP(&options.Query, "query", "q", "", "翻訳対象のJSONパス。 例: data[0].name")
	rootCmd.Flags().StringVar(&options.Translator, "translator", "gas", "翻訳エンジン")
	rootCmd.Flags().StringVarP(&options.SourceLanguage, "source", "s", "en", "翻訳元言語")
	rootCmd.Flags().StringVarP(&options.TargetLanguage, "target", "t", "ja", "翻訳先言語")
	rootCmd.Flags().IntVarP(&options.MaxParallel, "max-parallel", "p", 10, "最大並列数")
}

func readJSONFile(file string) (map[string]interface{}, error) {
	data, err := os.ReadFile(file)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	err = json.Unmarshal(data, &result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func writeJSONFile(file string, data map[string]interface{}) error {
	bytes, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return err
	}
	return os.WriteFile(file, bytes, 0644)
}

func translateJSON(data interface{}, querys []string, translator translator.Translator) (interface{}, error) {
	var (
		val interface{}
		err error
	)
	switch v := data.(type) {
	case map[string]interface{}:
		if querys[0] == "*" {
			uiprogress.Start()
			bar := uiprogress.AddBar(len(v))
			bar.AppendCompleted()
			bar.PrependElapsed()

			var wg sync.WaitGroup
			concurrency := options.MaxParallel
			ch := make(chan struct{}, concurrency)
			m := sync.Map{}

			for i := range v {
				wg.Add(1)
				ch <- struct{}{}
				go func(i string) {
					defer func() {
						<-ch
						wg.Done()
					}()

					val, err := translateJSON(v[i], querys[1:], translator)
					if err == nil {
						m.Store(i, val)
					} else {
						fmt.Printf("translate error: %v\n", err)
					}
					bar.Incr()
				}(i)
			}
			wg.Wait()

			m.Range(func(key, value interface{}) bool {
				v[key.(string)] = value
				return true
			})
		} else {
			if _, ok := v[querys[0]]; ok {
				val, err = translateJSON(v[querys[0]], querys[1:], translator)
				v[querys[0]] = val
			}
		}
	case []interface{}:
		if querys[0] == "*" {
			var wg sync.WaitGroup
			concurrency := options.MaxParallel
			ch := make(chan struct{}, concurrency)
			m := sync.Map{}

			for i := range v {
				wg.Add(1)
				ch <- struct{}{}
				go func(i int) {
					defer func() {
						<-ch
						wg.Done()
					}()

					val, err := translateJSON(v[i], querys[1:], translator)
					if err == nil {
						m.Store(i, val)
					} else {
						fmt.Printf("translate error: %v\n", err)
					}
				}(i)
			}
			wg.Wait()

			m.Range(func(key, value interface{}) bool {
				v[key.(int)] = value
				return true
			})
		} else {
			index, _ := strconv.Atoi(querys[0])
			if index < len(v) {
				val, err = translateJSON(v[index], querys[1:], translator)
				v[index] = val
			}
		}
	case string:
		data, err = translator.Translate(v)
	}
	return data, err
}

func generateTranslator(name string) (translator.Translator, error) {
	switch name {
	case "gas":
		return translator.NewGasTranslator(os.Getenv("GAS_DEPLOY_ID"), options.SourceLanguage, options.TargetLanguage)
	}
	return nil, fmt.Errorf("invalid translator name: %s", name)
}

func runRootCmd(cmd *cobra.Command, args []string) error {
	data, err := readJSONFile(options.File)
	if err != nil {
		return err
	}

	reg := regexp.MustCompile(`\[([0-9]*|\*)\]`)
	regrsult := reg.FindAllIndex([]byte(options.Query), -1)
	for _, v := range regrsult {
		index := options.Query[v[0]+1 : v[1]-1]
		options.Query = options.Query[:v[0]] + "." + index + options.Query[v[1]:]
	}

	translator, err := generateTranslator(options.Translator)
	if err != nil {
		return err
	}

	querys := strings.Split(options.Query, ".")
	res, err := translateJSON(data, querys, translator)
	if err != nil {
		return err
	}

	err = writeJSONFile(options.Output, res.(map[string]interface{}))
	if err != nil {
		return err
	}
	return nil
}
