package translator

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

type GasTranslator struct {
	gasDeployId string
	source      string
	target      string
}

func NewGasTranslator(gasDeployId, source, target string) (*GasTranslator, error) {
	if gasDeployId == "" {
		return nil, fmt.Errorf("環境変数GAS_DEPLOY_IDが未設定です。GASによる翻訳は、GAS_DEPLOY_IDを設定する必要があります。")
	}

	return &GasTranslator{
		gasDeployId: gasDeployId,
		source:      source,
		target:      target,
	}, nil
}

type GASResponse struct {
	Text string `json:"text"`
}

func (t GasTranslator) Translate(text string) (string, error) {
	url, err := url.Parse("https://script.google.com/macros/s/" + t.gasDeployId + "/exec")
	if err != nil {
		return "", err
	}

	q := url.Query()
	q.Set("text", text)
	q.Set("source", t.source)
	q.Set("target", t.target)
	url.RawQuery = q.Encode()

	resp, err := http.Get(url.String())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	byteArray, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var response GASResponse
	err = json.Unmarshal(byteArray, &response)
	if err != nil {
		return "", err
	}

	return response.Text, nil
}
