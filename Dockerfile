FROM golang:1.21.4-bullseye

WORKDIR /workspace

RUN apt update
RUN go install -v golang.org/x/tools/gopls@latest
RUN go install github.com/spf13/cobra-cli@latest

ENV GOPATH=/go/
