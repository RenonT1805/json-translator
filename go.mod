module gitlab.com/RenonT1805/json-translator

go 1.21.4

require (
	github.com/gosuri/uiprogress v0.0.1
	github.com/spf13/cobra v1.8.0
	gopkg.in/go-playground/validator.v9 v9.31.0
)

require (
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/gosuri/uilive v0.0.4 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/leodido/go-urn v1.4.0 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.6.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
